dear anh Hiệp,

Em là ToanLT4, ngày 20.09.2021 em đã làm những công việc sau:
- Đọc và tìm hiểu về requirements của mock project
 + nghiên cứu về luồng của ứng dụng
 + phân tích APIs cần sử dụng
 + lên kế hoạch làm việc
- tạo APIs trên themoviedb .org và test trên Postman
- Thiết kế giao diện Home screen trên android studio
Ngày 21.09.2021 của em là:
- Thiết kế giao diện Movie Detail screen.
- Thiết kế giao diện Search screen và Genres screen
- Sử dụng retrofit để call APIs

Kế hoạch ngày 22/09/2021 của em: 
- sử dụng retrofit để call APIs ở màn Home Screen

Ngày 22.09.2021 em đã làm những công việc sau:
- sử dụng Recylerview trong recylerview để hiện thị list movie trong tab Movies
- Chuyển style hiển thị

Kế hoạch ngày 23.09.2021 của em:
- Hiển thị thành công list movie và các list movie trong các category
- chuyển style hiển thị


Ngày 30.09.2021 em đã làm những công việc sau:
- code api genres và lấy hình ảnh genres từ api movie
- debug màn genres và api

Kế hoạch ngày 01.10.2021 của em:
- Hiển thị thành công genres và làm set start cho movie